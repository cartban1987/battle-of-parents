# Battle of parents

Construct 3 Oct 2020
 Contruct 3 game.

A game for all ages, arrange a battle for your parents, and find out who is better (there is a function of pressing both parents at the same time)

Implemented menu navigation
Many levels with questions
Each question is voiced and reproduced
There is a start over button
At the end of the game, a prize is given.

Added banner advertising and interstitial (google admob)

![alt text](https://gitlab.com/cartban1987/battle-of-parents/-/raw/main/menu.PNG)

![alt text](https://gitlab.com/cartban1987/battle-of-parents/-/raw/main/lvl1.PNG)

![alt text](https://gitlab.com/cartban1987/battle-of-parents/-/raw/main/Lvl9.PNG)
